<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biodata</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="GET">
        <label>First Name</label><br>
        <input type="text" name="firstname"> <br><br>
        <label>Last Name</label><br>
        <input type="text" name="lastname"> <br><br>
        <label>Gender :</label><br>
        <input type="radio" name="gen" value="male">Male <br>
        <input type="radio" name="gen" value="female">Female <br><br>
        <label>Nationality :</label><br>
        <select name="nationality">
            <option value="1">Indonesia</option>
            <option value="2">Inggris</option>
            <option value="3">Japan</option>
        </select><br><br>
        <label>Languange Spoken</label><br>
        <input type="checkbox" name="in">Bahasa Indonesia <br>
        <input type="checkbox" name="en">English <br>
        <input type="checkbox" name="other">Other <br><br>
        <label>Bio :</label><br>
        <textarea name="alamat" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">

    </form>
</body>
</html>